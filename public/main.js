import { createApp } from "./vue.esm-browser.prod.js";

function makeComponent(name, component) {
  return { name, component };
}

const strInput = {
  name: "str-input",
  component: {
    props: {
      modelValue: String,
    },
    emits: ["update:modelValue"],
    computed: {
      value: {
        get() {
          return this.modelValue;
        },
        set(value) {
          value = value !== "" ? value : undefined;
          this.$emit("update:modelValue", value);
        },
      },
    },
    template: `
  <input v-model="value" />`,
  },
};

const uintInput = {
  name: "uint-input",
  component: {
    props: {
      modelValue: [Number, String],
    },
    emits: ["update:modelValue"],
    computed: {
      value: {
        get() {
          return this.modelValue;
        },
        set(value) {
          if (value !== "" && isFinite(value) && value >= 0) {
            value = this.$el.value = Math.floor(value);
            this.$emit("update:modelValue", value);
          } else {
            this.$el.value = "";
            this.$emit("update:modelValue", undefined);
          }
        },
      },
    },
    template: `
  <input
    v-model="value"
    type="number"
    min="0"
    step="1" />`,
  },
};

const doubleInput = {
  name: "double-input",
  component: {
    props: {
      modelValue: [Number, String],
      params: Array,
    },
    emits: ["update:modelValue"],
    computed: {
      value: {
        get() {
          return this.modelValue;
        },
        set(value) {
          if (value !== "" && isFinite(value)) {
            this.$emit("update:modelValue", value);
          } else {
            this.$el.value = "";
            this.$emit("update:modelValue", undefined);
          }
        },
      },
    },
    template: `
  <input
    v-if="params !== undefined"
    v-model="value"
    type="number"
    :step="params[0]" />
  <input
    v-else
    v-model="value"
    type="number"
    :step="0.01" />`,
  },
};

const boolInput = {
  name: "bool-input",
  component: {
    props: {
      modelValue: Boolean,
    },
    emits: ["update:modelValue"],
    computed: {
      value: {
        get() {
          return this.modelValue;
        },
        set(value) {
          switch (value) {
            case "true":
              value = true;
              break;
            case "false":
              value = false;
              break;
            default:
              value = undefined;
              break;
          }
          this.$emit("update:modelValue", value);
        },
      },
    },
    template: `
  <select v-model="value">
    <option value="" />
    <option value="true">true</option>
    <option value="false">false</option>
  </select>`,
  },
};

const enumInput = {
  name: "enum-input",
  component: {
    props: {
      modelValue: String,
      params: Array,
    },
    emits: ["update:modelValue"],
    computed: {
      value: {
        get() {
          return this.modelValue;
        },
        set(value) {
          value = value !== "" ? value : undefined;
          this.$emit("update:modelValue", value);
        },
      },
    },
    template: `
  <select v-model="value">
    <option value="" />
    <option
      v-for="param in params"
      :key="param"
      :value="param">
      {{ param }}
    </option>
  </select>`,
  },
};

const binInput = {
  name: "bin-input",
  component: {
    data: function () {
      return {
        str: this.$_toStr(),
        isValid: undefined,
        updateModelValueByParent: true,
      };
    },
    props: {
      modelValue: Uint8Array,
    },
    emits: ["update:modelValue"],
    methods: {
      $_toStr: function () {
        if (this.modelValue === undefined) return "";

        function toHex(n, digit) {
          const tmp = "00" + n.toString(16).toUpperCase();
          return "h" + tmp.substring(tmp.length - digit);
        }

        const bin = this.modelValue;
        let str = "";
        const len = bin.byteLength - 1;

        str += toHex(bin[0], 2);
        if (len !== 0) str += " ";

        for (let i = 1; i < len; ++i) {
          str += toHex(bin[i], 2);
          str += i % 16 === 15 ? "\n" : " ";
        }

        if (len !== 0) str += toHex(bin[len], 2);

        return str;
      },
      $_updateModelValue: function () {
        this.updateModelValueByParent = false;

        if (this.str === "") {
          this.isValid = true;
          this.$emit("update:modelValue", undefined);
          return;
        }

        const splited = this.str.split(/[ \n]/);
        const buf = [];
        for (let i = 0; i < splited.length; ++i) {
          if (splited[i] === "") continue;

          let num = 0;
          if (splited[i][0] === "h" || splited[i][0] === "H") {
            num = parseInt(splited[i].slice(1), 16);
          } else if (splited[i][0] === "b" || splited[i][0] === "B") {
            num = parseInt(splited[i].slice(1), 2);
          } else {
            num = parseInt(splited[i], 10);
          }

          if (isNaN(num) || num < 0 || num >= 256) {
            this.isValid = false;
            this.$emit("update:modelValue", undefined);
            return;
          }

          buf.push(num);
        }

        this.isValid = true;
        this.$emit("update:modelValue", new Uint8Array(buf));
      },
    },
    watch: {
      modelValue: function () {
        if (this.updateModelValueByParent) this.str = this.$_toStr();
        this.updateModelValueByParent = true;
      },
    },
    template: `
  <textarea
    placeholder="dec -> 0-255\nhex -> h00-hff\nbin -> b00000000-b11111111\nseparate each value with a space or new line"
    style="font-family:'monospace, monospace'"
    rows="4"
    cols="64"
    v-model="str"
    @update:model-value="$_updateModelValue" />`,
  },
};

const objInput = {
  name: "obj-input",
  component: {
    data: function () {
      const raw = {};
      this.params.forEach((param) => (raw[param.name] = undefined));

      if (this.modelValue !== undefined) {
        for (const prop in raw) {
          if (prop in this.modelValue) raw[prop] = this.modelValue[prop];
        }
      }

      return { raw, updateModelValueByParent: true };
    },
    emits: ["update:modelValue"],
    props: {
      modelValue: Object,
      params: Array,
    },
    methods: {
      $_updateModelValue: function () {
        const obj = {};
        for (const prop in this.raw) {
          if (this.raw[prop] !== undefined) obj[prop] = this.raw[prop];
        }

        this.updateModelValueByParent = false;
        this.$emit(
          "update:modelValue",
          Object.keys(obj).length !== 0 ? obj : undefined
        );
      },
    },
    watch: {
      modelValue: function (newValue) {
        if (this.updateModelValueByParent) {
          for (const prop in this.raw) {
            this.raw[prop] = prop in newValue ? newValue[prop] : undefined;
          }
        }
        this.updateModelValueByParent = true;
      },
    },
    template: `
  <ul>
    <template 
      v-for="param in params"
      :key="param.name">
      <li>
        <a
          v-if="param.url !== undefined"
          :href="param.url"
          target="_blank"
          referrerpolicy="no-referrer">
          {{ param.name }}
        </a>
        <template v-else>
          {{ param.name }}
        </template>
        : 
        <component
          :is="param.type + '-input'"
          :params="param.params"
          v-model="raw[param.name]"
          @update:model-value="$_updateModelValue" />
      </li>
    </template>
  </ul>`,
  },
};

const webcodecsChecker = {
  name: "webcodecs-checker",
  component: {
    data: function () {
      return {
        config: {},
        msg: "",
      };
    },
    props: {
      target: String,
      params: Array,
    },
    mounted: function () {
      if (
        this.enable &&
        this.config !== undefined &&
        Object.keys(this.config).length !== 0
      ) {
        this.checkConfig();
      }
    },
    watch: {
      target: function () {
        this.config = {};
        this.msg = "";
      },
    },
    computed: {
      enable: function () {
        return (
          this.target in window && "isConfigSupported" in window[this.target]
        );
      },
      $_checkFunc: function () {
        return this.enable
          ? window[this.target]["isConfigSupported"]
          : undefined;
      },
    },
    methods: {
      checkConfig: function () {
        if (!this.enable) return;
        this.$_checkFunc(this.config)
          .then((ret) => {
            this.msg = ret.supported ? "supported!!!" : "unsupported...";
          })
          .catch((err) => {
            this.msg = err.message;
          });
      },
    },
    template: `
  <template v-if="enable">
    <obj-input
      :params="params"
      v-model="config"
      @update:model-value="checkConfig" />
    <div>{{ msg }}</div>
  </template>
  <div v-else>{{ target }} unsupported in this browser...</div>`,
  },
};

const audioEncoderConfig = {
  target: "AudioEncoder",
  params: [
    {
      name: "codec",
      type: "str",
      url: "https://www.w3.org/TR/webcodecs-codec-registry/#audio-codec-registry",
    },
    {
      name: "sampleRate",
      type: "uint",
      url: "https://www.w3.org/TR/webcodecs/#dom-audioencoderconfig-samplerate",
    },
    {
      name: "numberOfChannels",
      type: "uint",
      url: "https://www.w3.org/TR/webcodecs/#dom-audioencoderconfig-numberofchannels",
    },
    {
      name: "bitrate",
      type: "uint",
      url: "https://www.w3.org/TR/webcodecs/#dom-audioencoderconfig-bitrate",
    },
  ],
};

const audioDecoderConfig = {
  target: "AudioDecoder",
  params: [
    {
      name: "codec",
      type: "str",
      url: "https://www.w3.org/TR/webcodecs-codec-registry/#audio-codec-registry",
    },
    {
      name: "sampleRate",
      type: "uint",
      url: "https://www.w3.org/TR/webcodecs/#dom-audiodecoderconfig-samplerate",
    },
    {
      name: "numberOfChannels",
      type: "uint",
      url: "https://www.w3.org/TR/webcodecs/#dom-audiodecoderconfig-numberofchannels",
    },
    {
      name: "description",
      type: "bin",
      url: "https://www.w3.org/TR/webcodecs/#dom-audiodecoderconfig-description",
    },
  ],
};

const videoEncoderConfig = {
  target: "VideoEncoder",
  params: [
    {
      name: "codec",
      type: "str",
      url: "https://www.w3.org/TR/webcodecs-codec-registry/#video-codec-registry",
    },
    {
      name: "width",
      type: "uint",
      url: "https://www.w3.org/TR/webcodecs/#dom-videoencoderconfig-width",
    },
    {
      name: "height",
      type: "uint",
      url: "https://www.w3.org/TR/webcodecs/#dom-videoencoderconfig-height",
    },
    {
      name: "displayWidth",
      type: "uint",
      url: "https://www.w3.org/TR/webcodecs/#dom-videoencoderconfig-displaywidth",
    },
    {
      name: "displayHeight",
      type: "uint",
      url: "https://www.w3.org/TR/webcodecs/#dom-videoencoderconfig-displayheight",
    },
    {
      name: "bitrate",
      type: "uint",
      url: "https://www.w3.org/TR/webcodecs/#dom-videoencoderconfig-bitrate",
    },
    {
      name: "framerate",
      type: "double",
      url: "https://www.w3.org/TR/webcodecs/#dom-videoencoderconfig-framerate",
    },
    {
      name: "hardwareAcceleration",
      type: "enum",
      params: ["no-preference", "prefer-hardware", "prefer-software"],
      url: "https://www.w3.org/TR/webcodecs/#enumdef-hardwareacceleration",
    },
    {
      name: "alpha",
      type: "enum",
      params: ["keep", "discard"],
      url: "https://www.w3.org/TR/webcodecs/#enumdef-alphaoption",
    },
    {
      name: "scalabilityMode",
      type: "str",
      url: "https://www.w3.org/TR/webrtc-svc/#scalabilitymodes*",
    },
    {
      name: "bitrateMode",
      type: "enum",
      params: ["constant", "variable"],
      url: "https://www.w3.org/TR/mediastream-recording/#bitratemode",
    },
    {
      name: "latencyMode",
      type: "enum",
      params: ["quality", "realtime"],
      url: "https://www.w3.org/TR/webcodecs/#enumdef-latencymode",
    },
  ],
};

const videoDecoderConfig = {
  target: "VideoDecoder",
  params: [
    {
      name: "codec",
      type: "str",
      url: "https://www.w3.org/TR/webcodecs-codec-registry/#video-codec-registry",
    },
    {
      name: "codedWidth",
      type: "uint",
      url: "https://www.w3.org/TR/webcodecs/#dom-videodecoderconfig-codedwidth",
    },
    {
      name: "codedHeight",
      type: "uint",
      url: "https://www.w3.org/TR/webcodecs/#dom-videodecoderconfig-codedheight",
    },
    {
      name: "displayAspectWidth",
      type: "uint",
      url: "https://www.w3.org/TR/webcodecs/#dom-videodecoderconfig-displayaspectwidth",
    },
    {
      name: "displayAspectHeight",
      type: "uint",
      url: "https://www.w3.org/TR/webcodecs/#dom-videodecoderconfig-displayaspectheight",
    },
    {
      name: "colorSpace",
      type: "obj",
      params: [
        {
          name: "primaries",
          type: "enum",
          params: ["bt709", "bt470bg", "smpte170m"],
          url: "https://www.w3.org/TR/webcodecs/#enumdef-videocolorprimaries",
        },
        {
          name: "transfer",
          type: "enum",
          params: ["bt709", "smpte170m", "iec61966-2-1"],
          url: "https://www.w3.org/TR/webcodecs/#enumdef-videotransfercharacteristics",
        },
        {
          name: "matrix",
          type: "enum",
          params: ["rgb", "bt709", "bt470bg", "smpte170m"],
          url: "https://www.w3.org/TR/webcodecs/#enumdef-videomatrixcoefficients",
        },
        {
          name: "fullRange",
          type: "bool",
          url: "https://www.w3.org/TR/webcodecs/#videocolorspace-internal-slots",
        },
      ],
      url: "https://www.w3.org/TR/webcodecs/#dictdef-videocolorspaceinit",
    },
    {
      name: "hardwareAcceleration",
      type: "enum",
      params: ["no-preference", "prefer-hardware", "prefer-software"],
      url: "https://www.w3.org/TR/webcodecs/#enumdef-hardwareacceleration",
    },
    {
      name: "optimizeForLatency",
      type: "bool",
      url: "https://www.w3.org/TR/webcodecs/#dom-videodecoderconfig-optimizeforlatency",
    },
    {
      name: "description",
      type: "bin",
      url: "https://www.w3.org/TR/webcodecs/#dom-videodecoderconfig-description",
    },
  ],
};

const webcodecsSelector = {
  name: "webcodecs-selector",
  component: {
    data: function () {
      const list = [
        audioEncoderConfig,
        audioDecoderConfig,
        videoEncoderConfig,
        videoDecoderConfig,
      ];
      return {
        list,
        current: 0,
      };
    },
    props: {
      init: Object,
    },
    template: `
  <select v-model="current">
    <option
      v-for="(item, index) in list"
      :key="index"
      :value="index">
      {{ item.target }}
    </option>
  </select>
  <webcodecs-checker
    :target="list[current].target"
    :params="list[current].params" />`,
  },
};

const app = createApp({
  template: `<webcodecs-selector />`,
})
  .component(strInput.name, strInput.component)
  .component(uintInput.name, uintInput.component)
  .component(doubleInput.name, doubleInput.component)
  .component(boolInput.name, boolInput.component)
  .component(enumInput.name, enumInput.component)
  .component(binInput.name, binInput.component)
  .component(objInput.name, objInput.component)
  .component(webcodecsChecker.name, webcodecsChecker.component)
  .component(webcodecsSelector.name, webcodecsSelector.component);

app.mount("#app");
